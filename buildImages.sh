## This script is just for building the weatherapp images.

echo "Building images.."

echo "Building weatherapp_backend image..."

docker build -t kallen_weatherapp_backend ./backend/

echo "weatherapp_backend image built."

echo "Building weatherapp_backend image..."

docker build -t kallen_weatherapp_frontend ./frontend/

echo "weatherapp_frontend image built."

echo "weatherapp_backend and weatherapp_frontend images built."