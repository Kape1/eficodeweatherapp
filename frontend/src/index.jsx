import React from 'react';
import ReactDOM from 'react-dom';
import Map from './components/Map';

const baseURL = process.env.ENDPOINT;

const getWeatherFromApi = async (coords) => {
  try {
    const { latitude, longitude } = coords;
    const response = await fetch(`${baseURL}/weather?lat=${latitude}&lng=${longitude}`);
    return response.json();
  } catch (error) {
    console.error(error);
  }
  return {};
};

class Weather extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      icon: '',
      coords: {},
    };
  }

  async componentWillMount() {
    this.getWeatherFromLocation();
  }

  async getWeatherFromLocation() {
    // eslint-disable-next-line no-undef
    if (navigator.geolocation) {
      // eslint-disable-next-line no-undef
      navigator.geolocation.getCurrentPosition(async (position) => {
        this.setState({ coords: position.coords });
        const weather = await getWeatherFromApi(position.coords);
        const kelvin = -273.15;
        const celsius = parseFloat(weather.temp + kelvin).toFixed(2);
        this.setState({ temperature: celsius });
        this.setState({ icon: weather.icon.slice(0, -1) });
        this.setState({ city: weather.city });
        this.setState({ country: weather.country });
      }, err => console.log(err)
      );
    }
  }

  render() {
    const { icon, coords, temperature, city, country } = this.state;
    return (
      <div className="grid-container">
        <div className="heading">
          <h1>Weather in: {city}, {country}</h1>
        </div>
        <Map id="map" className="map" coords={coords} />
        <div className="weather">
          {icon && <img alt="weather-icon" src={`/img/${icon}.svg`} />}
          {temperature && <h3>Temp: {temperature}&deg;C</h3>}
        </div>
        <div className="footer">
          <h2>Kalle&apos;s WeatherApp</h2>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <div>
    <Weather />
  </div>,
  document.getElementById('app')
);
