import React, { Component } from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

export default class Map extends Component {

  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  componentDidMount() {
    this.map = L.map('map', {
      center: [1, 1],
      zoom: 12,
      zoomControl: false,
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      maxZoom: 20,
      maxNativeZoom: 17,
    }).addTo(this.map);
  }

  componentWillReceiveProps(props) {
    this.setState({ coords: props.coords });
    let cLat = 10;
    let cLng = 10;

    if (this.state != null) {
      const { latitude, longitude } = this.state.coords;
      cLat = latitude;
      cLng = longitude;
    }

    this.map.panTo({ lon: cLng, lat: cLat });
  }

  render() {
    return <div id="map" />;
  }

}
