# Weather app with docker


## Vagrantfile

* config.vm.network "forwarded_port", guest: 8000, host: 8000 
* config.vm.network "forwarded_port", guest: 8080, host: 8080 
* config.vm.network "forwarded_port", guest: 9000, host: 9000

* vb.memory = "8192"

* Start vm: vagrant up && vagrant ssh

## Deployment

* Add your own open weather API APPID inside the backend/.env file.
* You can also run the commands inside the scripts individually.

### Build images

1. Run: ./buildImages.sh

### Docker-compose deployment

1. Run: docker network create if_weatherapp
2. Run: docker-compose up

### Kubernetes install & deployment

1. Run: ./kubernetes/installKubernetes.sh
2. Run: ./kubernetes/deployToMinikube.sh
* To forward ports run these commands:
3. nohup kubectl port-forward service/weatherapp-backend --address 0.0.0.0 9000:9000 &
4. nohup kubectl port-forward service/weatherapp-frontend --address 0.0.0.0 8000:8000 &


# Changes I implemented into the source code.

## Backend src

1. Installed dotenv "npm i dotenv" for storing APPID and PORT environment vars into external .env file.
2. Fixed linter errors.

### Features:
1. /api/weather will now get weather based on location if coords object is passed. If not, it will use default Helsinki, FI location.
2. Get information like: location name, country code and temperature from the open weather api. 

## Frontend src

1. Installed dotenv "npm i dotenv" for storing ENDPOINT and PORT env vars into external .env file.
2. Fixed linter errors.

### Features:
1. Added navigator.geolocation to get browsers coordinates and pass them to backend.
2. Map and temperature are displayed on the UI.