const debug = require('debug')('weathermap');

require('dotenv').config();
const Koa = require('koa');
const router = require('koa-router')();
const fetch = require('node-fetch');
const cors = require('kcors');

const appId = process.env.APPID || '';
const mapURI = process.env.MAP_ENDPOINT || 'http://api.openweathermap.org/data/2.5';
const targetCity = process.env.TARGET_CITY || 'Helsinki,fi';

const port = process.env.PORT || 9000;

const app = new Koa();

app.use(cors());

const fetchWeather = async (coords) => {
  let endpoint = '';
  console.log(appId);
  
  if (coords.lat && coords.lng) {
    const { lat, lng, } = coords;
    console.log(`Fetching weather via coords lat: ${lat}, lng: ${lng}`);
    endpoint = `${mapURI}/weather?lat=${lat}&lon=${lng}&appid=${appId}&`;
  } else {
    console.log(`Fetching weather using default targetCity: ${targetCity}`);
    endpoint = `${mapURI}/weather?q=${targetCity}&appid=${appId}&`;
  }
  const response = await fetch(endpoint);
  return response ? response.json() : {};
};

router.get('/api/weather', async ctx => {
  console.log('Request made to /api/weather');
  console.log('ctx.request.query', ctx.request.query);
  const { lat, lng, } = ctx.request.query;
  const coords = {
    lat: lat,
    lng: lng,
  };
  const weatherData = await fetchWeather(coords);
  console.log(weatherData);
  const body = weatherData.weather ? weatherData.weather[0] : {};
  body.temp = weatherData.main.temp ? weatherData.main.temp : 10;
  body.city = weatherData.name;
  body.country = weatherData.sys.country;
  ctx.type = 'application/json; charset=utf-8';
  ctx.body = body;
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(port);

console.log(`App listening on port ${port}`);
