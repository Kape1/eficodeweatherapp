BACKEND_SERVICE=weatherapp-backend
FRONTEND_SERVICE=weatherapp-frontend
BACKEND_IMAGE=kallen_weatherapp_backend
FRONTEND_IMAGE=kallen_weatherapp_frontend

sudo minikube start --vm-driver=none
nohup sudo minikube dashboard &

nohup kubectl port-forward -n kubernetes-dashboard service/kubernetes-dashboard --address 0.0.0.0 8080:80 &

kubectl run $BACKEND_SERVICE --image=$BACKEND_IMAGE:latest --image-pull-policy=Never
kubectl run $FRONTEND_SERVICE --image=$FRONTEND_IMAGE:latest --image-pull-policy=Never

kubectl expose deployment $BACKEND_SERVICE --type=NodePort --port 9000
kubectl expose deployment $FRONTEND_SERVICE --type=NodePort --port 8000